#include <iostream>
#include <vector>
#include <algorithm>

// FILE CHECKS
#if __has_include("lab6.cpp")
#define LAB6
#elif __has_include("lab5.cpp")
#define LAB5
#elif __has_include("lab4.cpp")
#define LAB4
#elif __has_include("lab3.cpp")
#define LAB3
#endif

// HELPERS

/**
 * Asserts that [val] is true, and otherwise throws an exception with the [failureReport]
 * @param val The expression whose truth needs to be checked
 * @param failureReport The error message to provide in case of failure
 */
void assertTrue(bool val, std::string failureReport) {
    if (!val)
        throw std::runtime_error(failureReport);
}
void assertFalse(bool val, std::string failureReport) {
    assertTrue(!val,failureReport);
}

// STUBS

#ifdef LAB6
void e132();
void e133();
void p131();

#elifdef LAB5
void e107();
void p101();
void p102();
void e1112(); int maximum(std::vector<int> values);
void e1116(); std::vector<std::string> generate_substrings(std::string s);

#elifdef LAB4
void p88();
void e98();
void p92();
void p94();
void p916();

#elifdef LAB3

// add lab 3 stubs
void e61();
void e610(); bool same_elements(int a[], int b[], int size);
void p66();
void e74(); double* maximum(double* a, int size);
void e715();
void e79(); //char* replace_all(const char s[], const char t[], const char u[]);
#endif

// TESTS
#ifdef LAB6

#elifdef LAB5
void teste1112() {
    std::vector<int> t1{1,2,3,4,5};
    std::vector<int> t2{3,1,4,1,5,9,2,6,5,3,5,8,9,7,9,3,2,3};
    assertTrue(maximum(t1)==5,"Failed to find the maximum of {1,2,3,4,5}");
    assertTrue(maximum(t2)==9,"Failed to find the maximum of {3,1,4,1,5,9,2,6,5,3,5,8,9,7,9,3,2,3");
};
void teste1116() {
    auto rum =  generate_substrings("rum");

//    std::vector<std::string> tmp = generate_substrings("rum");
//    for (std::string str: tmp) {
//        std::cout << "'" << str << "'" << std::endl;
//    }

    assertTrue(std::find(rum.begin(),rum.end(),std::string("m"))!=rum.end(),"Could not find 'm' in substrings of 'rum'.");
    assertTrue(std::find(rum.begin(),rum.end(),std::string("rum"))!=rum.end(),"Could not find 'rum' in substrings of 'rum'.");
    assertTrue(std::find(rum.begin(),rum.end(),std::string(""))!=rum.end(),"Could not find '' in substrings of 'rum'.");
    assertTrue(rum.size()==7,"'rum' has 7 substrings, but "+std::to_string(rum.size())+" found.");
}
#elifdef LAB4

#elifdef LAB3
void testE610() {
  // run the test cases from the book
  int a[]={1,4,9,16,9,7,4,9,11};
  int b[]={11,1,4,9,16,9,7,4,9};
  int c[]={11,11,7,9,16,4,1,4,9};
  int d[]={};

  assertTrue(same_elements(a,b, 9), "Failed first book test - 1 4 9 16 9 7 4 9 11 should be the same as 1 4 9 16 9 7 4 9 11.");
  assertFalse(same_elements(a,c, 9), "Failed first book test - 1 4 9 16 9 7 4 9 11 should NOT be the same as 11 11 7 9 16 4 1 4 9.");
  assertTrue( same_elements(d,d,0), "same_elements on two empty lists should return true");
}

void testE74() {
  double nullList[]={};
  double simpleList[]={1,2,3,4,5};
  double complexList[]={-1.0, 2.3, -4.3, -2.3, 4.3, 3.2};

  assertTrue(maximum((double *){},0) == nullptr,"maximum of empty list should return nullptr");
  assertTrue(*maximum(simpleList ,5) == 5,"maximum of {1,2,3,4,5} should return 5");
  assertTrue(*maximum(complexList ,6) == 4.3,"maximum of {-1.0, 2.3, -4.3, -2.3, 4.3, 3.2} should return 4.3");
}

void testE79() {
  // to do
}
#endif

// MAIN
int main() {
#ifdef LAB6
    e132();
    e133();
    p131();
#elifdef LAB5
    e107();
    p101();
    p102();
    e1112(); teste1112();
    e1116(); teste1116();
#elifdef LAB4
    p88();
    e98();
    p92();
    p94();
    p916();
#elifdef LAB3
    // lab 3 tests
  e61(); testE610();
  p66();
  e74(); testE74();
  e715();
  e79(); testE79();
#endif
}